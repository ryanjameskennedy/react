# Git
### Check to make sure that Git is installed and available
```
git --version
```

### Configure your user name to be used by Git
```
git config --global user.name "Name"
```

### Configure your email to be used by Git
```
git config --global user.email <email address>
```

### Initialize the folder as a Git repository
```
cd path/to/folder
git init
```

### Check your Git repository's status
```
git status
```

### Add files to the staging area of your Git repository
```
git add .
```

### Commit the current staging area to your Git repository (with message)
```
git commit -m "commit message"
```

### Check the log of the commits to your Git repository
```
git log --oneline
```

### Check out a file from a different commit, find the number of the desired commit using the git log
```
git checkout <desired commit's number> <filename>
```

### Discard the effect of the previous operation and restore the file to its state
```
git reset HEAD <filename>
git checkout -- <filename>
```

### Create a new branch and switch to it at the same time
```
git checkout -b <branch_name>
```

### Set up your local repository to link to your online Git repository
```
git remote add origin <repository URL>
```

### Change remote repository
```
git remote set-url origin <repository URL>
```

### Push the commits to the online repository
```
git push -u origin master
```

### Clone an online repository to your computer
```
git clone <online repository>
```

# Node.js
## Download and install the current version of Node from the [Node.js webpage](https://nodejs.org)

### Initiate Node package manager (NPM)
```
npm init
```
### Everything can be left as default but make sure that your entry point filename matches your index filename (index.html) and that you have linked your online Git repository

### Install lite-server
```
npm install lite-server --save-dev
```

### Visualise index.html
```
npm run lite-server
```

### Git ignore node modules
```
echo node_modules > .gitignore
```

# Yarn
### Install Yarn using npm
```
sudo npm install -g yarn
```

### Use Yarn on a by-project basis
```
cd ~/path/to/project
```

### "Berry" is the codename for the Yarn 2 release line
```
yarn set version berry
```

### Update Yarn to the latest version
```
yarn set version latest
```

### Add create-react-app to global
```
sudo yarn global add create-react-app
```

### Create first react app called confusion
```
create-react-app confusion
```

### Start the application
```
cd confusion
yarn start
```

### Configure your React project to use Reactstrap
```
yarn add bootstrap
yarn add reactstrap
yarn add react-popper
```

### Configure your React project to use Reactstrap
```
yarn add font-awesome
yarn add bootstrap-social
```

### Installing and Configuring React Router
```
yarn add react-router-dom
```

### Install Redux and React-Redux
```
yarn add redux
yarn add react-redux
```

### Install the react-redux-form
```
yarn add react-redux-form
```

### Install Redux Thunk and Logger
```
yarn add redux-thunk
yarn add redux-logger
```

### Create a JSON server
```
sudo npm install -g json-server
```

### Configure the server
```
json-server --watch db.json -p 3001 -d 2000
```

### Install Fetch
```
yarn add cross-fetch
```

### Install react-transition-group
```
yarn add react-transition-group
```

### Install react-animation-components
```
yarn add react-animation-components
yarn add prop-types
```

### Build the distribution folder containing your React application,
```
yarn build
```

### Copy all contents in build folder into the json-server/public folder and react will be served using server.
